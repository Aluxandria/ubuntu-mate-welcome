# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-19 00:53+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ubuntu-mate-welcome:77
msgid "Software Boutique"
msgstr ""

#: ubuntu-mate-welcome:78
msgid ""
"Software changes are in progress. Please allow them to complete before "
"closing Welcome."
msgstr ""

#: ubuntu-mate-welcome:79
msgid "OK"
msgstr ""

#: ubuntu-mate-welcome:140
msgid "Fixing incomplete install succeeded"
msgstr ""

#: ubuntu-mate-welcome:141
msgid "Successfully fixed an incomplete install."
msgstr ""

#: ubuntu-mate-welcome:141
msgid "Fixing the incomplete install was successful."
msgstr ""

#: ubuntu-mate-welcome:145
msgid "Fixing incomplete install failed"
msgstr ""

#: ubuntu-mate-welcome:146
msgid "Failed to fix incomplete install."
msgstr ""

#: ubuntu-mate-welcome:146
msgid "Fixing the incomplete install failed."
msgstr ""

#: ubuntu-mate-welcome:154
msgid "Fixing broken dependencies succeeded"
msgstr ""

#: ubuntu-mate-welcome:155
msgid "Successfully fixed broken dependencies."
msgstr ""

#: ubuntu-mate-welcome:155
msgid "Fixing the broken dependencies was successful."
msgstr ""

#: ubuntu-mate-welcome:159
msgid "Fixing broken dependencies failed"
msgstr ""

#: ubuntu-mate-welcome:160
msgid "Failed to fix broken dependencies."
msgstr ""

#: ubuntu-mate-welcome:160
msgid "Fixing the broken dependencies failed."
msgstr ""

#: ubuntu-mate-welcome:217
msgid "Install"
msgstr ""

#: ubuntu-mate-welcome:218
msgid "Installation of "
msgstr ""

#: ubuntu-mate-welcome:219
msgid "installed."
msgstr ""

#: ubuntu-mate-welcome:221
msgid "Remove"
msgstr ""

#: ubuntu-mate-welcome:222
msgid "Removal of "
msgstr ""

#: ubuntu-mate-welcome:223
msgid "removed."
msgstr ""

#: ubuntu-mate-welcome:225
msgid "Upgrade"
msgstr ""

#: ubuntu-mate-welcome:226
msgid "Upgrade of "
msgstr ""

#: ubuntu-mate-welcome:227
msgid "upgraded."
msgstr ""

#: ubuntu-mate-welcome:232 ubuntu-mate-welcome:233
msgid "complete"
msgstr ""

#: ubuntu-mate-welcome:233
msgid "has been successfully "
msgstr ""

#: ubuntu-mate-welcome:235 ubuntu-mate-welcome:236
msgid "cancelled"
msgstr ""

#: ubuntu-mate-welcome:236
msgid "was cancelled."
msgstr ""

#: ubuntu-mate-welcome:238 ubuntu-mate-welcome:239
msgid "failed"
msgstr ""

#: ubuntu-mate-welcome:239
msgid "failed."
msgstr ""

#: ubuntu-mate-welcome:389
msgid "Blu-ray AACS database install succeeded"
msgstr ""

#: ubuntu-mate-welcome:390
msgid "Successfully installed the Blu-ray AACS database."
msgstr ""

#: ubuntu-mate-welcome:390
msgid "Installation of the Blu-ray AACS database was successful."
msgstr ""

#: ubuntu-mate-welcome:393
msgid "Blu-ray AACS database install failed"
msgstr ""

#: ubuntu-mate-welcome:394
msgid "Failed to install the Blu-ray AACS database."
msgstr ""

#: ubuntu-mate-welcome:394
msgid "Installation of the Blu-ray AACS database failed."
msgstr ""
