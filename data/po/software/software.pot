#
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: : LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: software.html:17
msgid "Software"
msgstr ""

#: software.html:17
msgid "Boutique"
msgstr ""

#: software.html:46
msgid "Software Boutique"
msgstr ""

#: software.html:47
msgid ""
"There is an abundance of software available for Ubuntu MATE and some people "
"find that choice overwhelming. The Boutique is a carefully curated selection"
" of the best-in-class applications chosen because they integrate well, "
"complement Ubuntu MATE and enable you to self style your computing "
"experience."
msgstr ""

#: software.html:54
msgid "If you can't find what you're looking for, install one of the"
msgstr ""

#: software.html:56
msgid "software centers"
msgstr ""

#: software.html:57
msgid "to explore the complete Ubuntu software catalog."
msgstr ""

#: software.html:61
msgid "More Apps Await."
msgstr ""

#: software.html:62
msgid ""
"Connect your computer to the Internet to explore a wide selection of "
"applications for Ubuntu MATE."
msgstr ""

#: software.html:64
msgid ""
"Once online, you'll be able to download and install tried & tested software "
"right here in Welcome. Our picks ensure that the software featured "
"integrates well the Ubuntu MATE desktop."
msgstr ""

#: software.html:70
msgid ""
"Sorry, Welcome could not establish a connection to the Ubuntu repository "
"server. Please check your connection and try again."
msgstr ""

#: software.html:75
msgid "Retry Connection"
msgstr ""

#: software.html:79
msgid "Connection Help"
msgstr ""

#: software.html:85
msgid "Get Connected."
msgstr ""

#: software.html:86
msgid ""
"Here's a few things you can check, depending on the type of connection you "
"have:"
msgstr ""

#: software.html:87
msgid "Wired Connection"
msgstr ""

#: software.html:89
msgid "Is the cable securely plugged in?"
msgstr ""

#: software.html:90
msgid ""
"Is the router online and can other devices access the network? Try "
"restarting the router."
msgstr ""

#: software.html:93
msgid "Wireless Connection"
msgstr ""

#: software.html:94
msgid "Have you entered the correct wireless password from the"
msgstr ""

#: software.html:94
msgid "network indicator"
msgstr ""

#: software.html:94
msgid "in the upper-right?"
msgstr ""

#: software.html:96
msgid "When disconnected, the applet looks like this:"
msgstr ""

#: software.html:98
msgid ""
"Is there a physical switch that may have accidentally switched off wireless "
"functionality?"
msgstr ""

#: software.html:100
msgid "Is there a soft (keyboard) switch that toggles it off?"
msgstr ""

#: software.html:102
msgid "You may need to hold the FN key while pressing the function key."
msgstr ""

#: software.html:103
msgid "Here's an example:"
msgstr ""

#: software.html:107
msgid "Not working at all or experiencing sluggish connections?"
msgstr ""

#: software.html:108
msgid ""
"Sorry to hear that. You will need a temporary wired connection to install "
"working drivers."
msgstr ""

#: software.html:110
msgid "The"
msgstr ""

#: software.html:110
msgid "Additional Drivers"
msgstr ""

#: software.html:110
msgid "tab may have them available for your system."
msgstr ""

#: software.html:111
msgid ""
"Otherwise, you will need to manually install third party drivers for your "
"hardware, or a download specific package containing the firmware. See"
msgstr ""

#: software.html:112
msgid "Drivers"
msgstr ""

#: software.html:112
msgid "in the"
msgstr ""

#: software.html:112
msgid "Getting Started"
msgstr ""

#: software.html:113
msgid "section for more details."
msgstr ""

#: software.html:114
msgid "Feel free to"
msgstr ""

#: software.html:114
msgid "ask the community"
msgstr ""

#: software.html:114
msgid "if you need assistance."
msgstr ""

#: software.html:119
msgid "OK"
msgstr ""

#: software.html:131
msgid "Accessories"
msgstr ""

#: software.html133, 146, 159, 172, 185, 198, 211, 224, 237
msgid "No Filter"
msgstr ""

#: software.html:135
msgid "Handy utilities for your computing needs."
msgstr ""

#: software.html:144
msgid "Education"
msgstr ""

#: software.html:148
msgid "For study and children."
msgstr ""

#: software.html:157
msgid "Games"
msgstr ""

#: software.html:161
msgid "A selection of 2D and 3D games for your enjoyment."
msgstr ""

#: software.html:170
msgid "Graphics"
msgstr ""

#: software.html:174
msgid "For producing and editing works of art."
msgstr ""

#: software.html:183
msgid "Internet"
msgstr ""

#: software.html:187
msgid "For staying connected and enjoying the features of your own cloud."
msgstr ""

#: software.html:196
msgid "Office"
msgstr ""

#: software.html:200
msgid "For more then just documents and spreadsheets."
msgstr ""

#: software.html:209
msgid "Programming"
msgstr ""

#: software.html:213
msgid "For the developers and system administrators out there."
msgstr ""

#: software.html:222
msgid "Sound & Video"
msgstr ""

#: software.html:226
msgid "Multimedia software for listening and production."
msgstr ""

#: software.html:235
msgid "System Tools"
msgstr ""

#: software.html:239
msgid "Software that makes the most out of your system resources."
msgstr ""

#: software.html:248
msgid "Universal Access"
msgstr ""

#: software.html:249
msgid "Software that makes your computer more accessible."
msgstr ""

#: software.html:258
msgid "Servers"
msgstr ""

#: software.html:259
msgid "One-click installations for serving the network."
msgstr ""

#: software.html:268
msgid "Discover More Software"
msgstr ""

#: software.html:269
msgid ""
"Graphical interfaces to browse a wide selection of software available for "
"your operating system."
msgstr ""

#: software.html:277
msgid "Miscellaneous Fixes"
msgstr ""

#: software.html:278
msgid ""
"This section contains operations that can fix common problems should you "
"encounter an error while upgrading or installing new software."
msgstr ""

#: software.html:285
msgid "Outdated Package Lists?"
msgstr ""

#: software.html:286
msgid "Your repository lists may be out of date, which can cause"
msgstr ""

#: software.html:286
msgid "Not Found"
msgstr ""

#: software.html:286
msgid "errors and"
msgstr ""

#: software.html:286
msgid "outdated version"
msgstr ""

#: software.html:287
msgid ""
"information when trying to install new or newer versions of software. This "
"is particularly the case when Ubuntu MATE connects online for the first time"
" after installation."
msgstr ""

#: software.html:290
msgid "Update the repository lists:"
msgstr ""

#: software.html:294
msgid "Update Sources List"
msgstr ""

#: software.html:295
msgid "Upgrade Installed Packages"
msgstr ""

#: software.html:299
msgid "Broken Packages?"
msgstr ""

#: software.html:300
msgid "When a previous installation or removal was interrupted"
msgstr ""

#: software.html:301
msgid "(for instance, due to power failure or loss of Internet connection)"
msgstr ""

#: software.html:302
msgid ""
"further software cannot be added or removed without properly re-configuring "
"these broken packages. If necessary, you may need to also resolve "
"dependencies and install any missing packages in order for the software to "
"run properly."
msgstr ""

#: software.html:308
msgid "Configure packages that were unpacked but not configured:"
msgstr ""

#: software.html:312
msgid "Download and install broken dependencies:"
msgstr ""

#: software.html:316
msgid "Configure Interrupted Packages"
msgstr ""

#: software.html:317
msgid "Resolve Broken Packages"
msgstr ""

#: software.html:321
msgid ""
"In addition, listed below are terminal equivalent commands that otherwise "
"appear in the"
msgstr ""

#: software.html:322
msgid "Software Updater"
msgstr ""

#: software.html:324
msgid "Upgrade all packages that have a new version available:"
msgstr ""

#: software.html:328
msgid "Upgrade to a new release of Ubuntu MATE:"
msgstr ""

#: software.html:342
msgid "Filter by Software License"
msgstr ""

#: software.html:343
msgid "Hide Proprietary Software"
msgstr ""

#: software.html:345
msgid "Show Terminal Commands"
msgstr ""

#: software.html:351
msgid "Proprietary Software"
msgstr ""

#: software.html:352
msgid ""
"is owned by an individual or company. There may be restrictions on its usage"
" (like a license agreement), and provides no access to the source code."
msgstr ""

#: software.html:355
msgid "Free and Open Source Software"
msgstr ""

#: software.html:356
msgid ""
"grants the user the freedom to share, study and modify the software without "
"restriction. For instance, the GPL is an example of a 'Free Software' "
"license."
msgstr ""
